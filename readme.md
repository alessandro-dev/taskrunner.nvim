# Taskrunner.nvim

Task runner for Neovim, based on [asyncrun.vim](https://github.com/skywind3000/asyncrun.vim/) and [yabs.nvim](https://github.com/pianocomposer321/yabs.nvim).

## Installation

```lua
  use({
    "https://gitlab.com/alessandro-dev/taskrunner.nvim",
    requires = { "nvim-lua/plenary.nvim", "nvim-telescope/telescope.nvim" },
    rocks = {
      { "lyaml", server = "http://rocks.moonscript.org", version = "6.2.7" },
    },
    config = function()
      require("taskrunner").config({})
    end,
  })
```

Telescope is not required, but it will allow you to use it as a fuzzy picker for the tasks.

## Usage

<!-- TODO(2022-05-10): add usage documentation -->

## TODO

- Create vim command for running tasks without telescope
