local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local actions = require("telescope.actions")
local utils = require("telescope.utils")
local action_set = require("telescope.actions.set")
local action_state = require("telescope.actions.state")
local conf = require("telescope.config").values
local strings = require("plenary.strings")

local taskrunner = require("taskrunner")

local telescope_taskrunner = function(opts)
	local tasks = vim.tbl_values(taskrunner.get_tasks())
	local widths = { name = 0, command = 0 }

	vim.tbl_map(function(entry)
		widths.name = math.max(widths.name, strings.strdisplaywidth(entry.name or ""))
		widths.command = math.max(widths.command, strings.strdisplaywidth(entry.command or ""))
	end, tasks)

	local displayer = require("telescope.pickers.entry_display").create({
		separator = " ",
		items = { { width = widths.name }, { width = widths.command } },
	})

	local function make_display(entry)
		return displayer({ { entry.value.name, "Special" }, { entry.value.command } })
	end

	pickers.new(opts or {}, {
		prompt_title = "Taskrunner",
		finder = finders.new_table({
			results = tasks,
			entry_maker = function(entry)
				return {
					value = entry,
					ordinal = entry.name,
					display = make_display,
				}
			end,
		}),
		attach_mappings = function()
			action_set.select:replace(function(prompt_bufnr)
				actions.close(prompt_bufnr)
				local selection = action_state.get_selected_entry()
				taskrunner.dispatch(selection.value)
			end)

			return true
		end,
		sorter = conf.generic_sorter(opts or {}),
	}):find()
end

return require("telescope").register_extension({
	exports = {
		taskrunner = telescope_taskrunner,
	},
})
