local M = {}

local config = require("taskrunner.config")

local outputs = {
	quickfix = require("taskrunner.outputs.quickfix"),
}

function M.by_task(task)
	local output = task.output
	if output == nil or output == "" then
		output = config.get().output
	end

	return outputs[output]
end

return M
