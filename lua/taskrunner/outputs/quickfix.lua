-- local M = {}

local utils = require("taskrunner.utils")
local p_utils = require("plenary.async.util")

local Output = utils.class(function(self, processors)
	self.processors = processors
	self._results = {}
	vim.fn.setqflist({}, "r", { lines = {} })
end)

function Output:emit(data)
	for _, processor in ipairs(self.processors) do
		data = processor(data)
	end

	p_utils.scheduler(function()
		vim.fn.setqflist({}, "a", { lines = { data } })
	end)
end

function Output:close() end

return Output
