local Job = require("plenary.job")

local config = require("taskrunner.config")
local utils = require("taskrunner.utils")

local Runner = utils.class(function(self, task, output, posts)
	local cfg = config.get()

	local function onread(_, data)
		for _, line in ipairs(utils.str_split(data, "\n")) do
			output:emit(line)
		end
	end

	self._job = Job:new({
		command = cfg.shell,
		args = { unpack(cfg.shell_flags), task.command },
		env = task.env,
		cwd = task.cwd,
		on_stdout = onread,
		on_stderr = onread,
		on_exit = function(code, signal)
			output:close()
			for _, post in ipairs(posts) do
				post({ exit_code = signal, task_name = task.name })
			end
		end,
	})
end)

function Runner:run()
	self._job:start()
end

return Runner
