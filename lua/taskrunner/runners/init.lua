local M = {}

local config = require("taskrunner.config")

local runners = {
	plenary = require("taskrunner.runners.plenary"),
}

function M.by_task(task)
	local runner = task.runner
	if runner == nil or runner == "" then
		runner = config.get().runner
	end

	return runners[runner]
end

return M
