local M = {}

function M.str_split(input, sep)
	if sep == nil then
		sep = "%s"
	end
	local t = {}
	for str in string.gmatch(input, "([^" .. sep .. "]+)") do
		table.insert(t, str)
	end
	return t
end

function M.str_startswith(str, start)
	return str:sub(1, #start) == start
end

function M.class(base, init)
	local c = {} -- a new class instance
	if not init and type(base) == "function" then
		init = base
		base = nil
	elseif type(base) == "table" then
		-- our new class is a shallow copy of the base class!
		for i, v in pairs(base) do
			c[i] = v
		end
		c._base = base
	end
	-- the class will be the metatable for all its objects,
	-- and they will look up their methods in it.
	c.__index = c

	-- expose a constructor which can be called by <classname>(<args>)
	local mt = {}
	mt.__call = function(class_tbl, ...)
		local obj = {}
		setmetatable(obj, c)
		if init then
			init(obj, ...)
		else
			-- make sure that any stuff from the base class is initialized!
			if base and base.init then
				base.init(obj, ...)
			end
		end
		return obj
	end
	c.init = init
	c.is_a = function(self, klass)
		local m = getmetatable(self)
		while m do
			if m == klass then
				return true
			end
			m = m._base
		end
		return false
	end
	setmetatable(c, mt)
	return c
end

function M.noop() end

function M.parse_cmd_and_args(command_str)
	local split = M.str_split(command_str)
	local cmd = split[1]
	local args = {}

	if #split > 1 then
		args = { unpack(split, 2, #split) }
	end

	return cmd, args
end

return M
