local M = {}

function M.notify(data)
	local status = data.exit_code == 0 and "success" or "error"
	local level = data.exit_code == 0 and vim.log.levels.INFO or vim.log.levels.ERROR
	vim.notify(("Task [%s] finsished with status: %s"):format(data.task_name, status), level)
end

return M
