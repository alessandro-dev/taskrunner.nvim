local M = {}

function M.strip_ansi(line)
	return line
		:gsub("\x1b%[%d+;%d+;%d+;%d+;%d+m", "")
		:gsub("\x1b%[%d+;%d+;%d+;%d+m", "")
		:gsub("\x1b%[%d+;%d+;%d+m", "")
		:gsub("\x1b%[%d+;%d+m", "")
		:gsub("\x1b%[%d+m", "")
end

return M
