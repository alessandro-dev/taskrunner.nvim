local config = require("taskrunner.config")
local output = require("taskrunner.outputs")
local runners = require("taskrunner.runners")
local utils = require("taskrunner.utils")

local function make_processors(task)
	local processors = config.get().processors
	return vim.tbl_map(function(processor)
		return processors[processor]
	end, task.processors)
end

local function make_posts(task)
	local posts = config.get().posts
	return vim.tbl_map(function(processor)
		return posts[processor]
	end, task.posts)
end

local Task = utils.class(function(self, task)
	self.command = task.command
	self.cwd = task.cwd
	self.env = task.env
	self.name = task.name

	self.output = output.by_task(task)(make_processors(task))
	self.runner = runners.by_task(task)(self, self.output, make_posts(task))
end)

function Task:dispatch()
	self.runner:run(self)
end

return Task
