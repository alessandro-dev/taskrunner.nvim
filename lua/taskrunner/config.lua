local M = {}

local Path = require("plenary.path")
local processors = require("taskrunner.processors")
local posts = require("taskrunner.posts")

local default_config = {
	global_tasks_file = Path:new(vim.fn.stdpath("data")):joinpath("taskrunner.yaml").filename,
	local_task_file = ".taskrunner",
	shell = vim.opt.shell:get(),
	shell_flags = { "-c" },
	runner = "plenary",
	output = "quickfix",
	processors = processors,
	posts = posts,
	macros = {
		["%$%(VIM_CWD%)"] = vim.loop.cwd(),
		["%$%(VIM_FILEPATH%)"] = vim.fn.expand("%:p"),
		["%$%(VIM_FILENAME%)"] = vim.fn.expand("%:t"),
		["%$%(VIM_FILEDIR%)"] = vim.fn.expand("%:p:h"),
		["%$%(VIM_FILENOEXT%)"] = vim.fn.expand("%:t:r"),
		["%$%(VIM_FILEEXT%)"] = "." .. vim.fn.expand("%:e"),
		["%$%(HOME%)"] = vim.loop.os_homedir(),
	},
}

M._config = default_config

function M.set(opts)
	M._config = vim.tbl_deep_extend("force", M._config, opts)
end

function M.get()
	return M._config
end

function M.reset()
	M._config = default_config
end

function M.register_processor(name, processor)
	M._config.processors[name] = processor
end

function M.register_post(name, post)
	M._config.posts[name] = post
end

return M
