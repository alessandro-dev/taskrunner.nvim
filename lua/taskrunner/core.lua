local M = {}

local config = require("taskrunner.config")

local Path = require("plenary.path")
local yaml = require("lyaml")

local function tasks_files()
	local cfg = config.get()
	local cwd = Path:new(vim.loop.cwd())
	local files = {}
	local global_file = Path:new(cfg.global_tasks_file)

	if global_file:exists() and global_file:is_file() then
		table.insert(files, global_file.filename)
	end

	local paths = { cwd.filename, unpack(cwd:parents()) }
	for _, p in ipairs(paths) do
		local path = Path:new(p):joinpath(cfg.local_task_file)
		if path:exists() and path:is_file() then
			table.insert(files, path.filename)
			break
		end
	end

	return files
end

local function parse_task_file(filename)
	local parsed = yaml.load(Path:new(filename):read()) or {}
	for key, value in pairs(parsed) do
		if value.name == nil or value.name == "" then
			parsed[key].name = key
		end

		for macro, text in pairs(config.get().macros) do
			parsed[key].command = value.command:gsub(macro, text)
			parsed[key].cwd = (value.cwd or vim.loop.cwd()):gsub(macro, text)
		end

		parsed[key].env = vim.tbl_extend("keep", {}, {}, unpack(value.env or {}))
	end

	return parsed
end

function M.get_tasks()
	local tasks = vim.tbl_map(parse_task_file, tasks_files())
	return vim.tbl_extend("force", {}, {}, unpack(tasks))
end

return M
