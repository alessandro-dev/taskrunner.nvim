local M = {}

local config = require("taskrunner.config")
local core = require("taskrunner.core")
local Task = require("taskrunner.tasks")

M.config = config.set
M.get_tasks = core.get_tasks

function M.dispatch(opts)
	Task(opts):dispatch()
end

return M
